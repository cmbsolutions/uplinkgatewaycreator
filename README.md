# Uplink Gateway Creator #

### What is this repository for? ###

* This small tool will let you create your own gateways for the game Uplink Hacker Elite

### How do I get set up? ###

* Clone the source
* Open Visual Studio 2017
* Restore NuGet packages
* Build and run

#### OR ####

* Download latest binary
* Extract into a folder of your own choosing
* Overwrite older version (if present)
* Run the UplinkGatewayCreator.exe

### Contribution guidelines ###

* Feel free to fork and modify the source, would love to see a pull-request of other people ;)
* I'll do a basic review on pull-request, eg. test if the master repo will still build when pull-request is merged
* A short message of what you added and/or changed wuld be nice.
